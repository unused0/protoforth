all : FORTH

FORTH : build_sim create_system_disk create_user_disk assemble loadcards 

build_sim : syms.h
	$(MAKE) -C ./ibm1130

create_system_disk :
	cp dms.dsk.dist dms.dsk
	chmod +w dms.dsk

create_user_disk : create_system_disk
	-./create_user_disk.sh

assemble :
	-./assemble_forth.sh

loadcards : load_cards
	-./load_cards <FORTH.cards

extrsyms : extr_syms

syms.h :
	$(MAKE) extr_syms
	./extr_syms

load_cards: load_cards.c
	$(CC) load_cards.c -o load_cards

extr_syms: extr_syms.c
	$(CC) extr_syms.c -o extr_syms -Wall

clean:
	-@rm -rf extr_syms ibm1130/ibm1130 create_user_disk.deck create_user_disk.lst dms.dsk FORTH.lst assemble.deck forth.dsk.initialized load_cards 2>/dev/null || true
