#!/bin/bash
IBM1130=./ibm1130/ibm1130
# card images start at cyl 29, go to 199, so 171 cyls
# cyl 0  id, dcom, let
# cyl 1  flet
# cyl 2-199 fixed area (198 cyls)
# 198 gives error; only 197 availible
cat > create_user_disk.deck <<END
// JOB    1234 3333
// DUP
*DEFINE FIXED AREA         197      3333
END
#cat < FORTH.cards >> create_user_disk.deck
rm create_user_disk.lst
rm forth.dsk.initialized
$IBM1130 <<END
att prt create_user_disk.lst
att dsk forth.dsk.initialized
reset
deposit ces 0
load zdcip.out
go
deposit ces 0200
go
deposit ces 0
go
deposit ces 3333
go
go
deposit ces 0
go
det dsk
reset
att dsk dms.dsk
att dsk1 forth.dsk.initialized
att cr create_user_disk.deck
boot dsk
det prt
quit
END

