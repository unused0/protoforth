#!/bin/bash
IBM1130=./ibm1130/ibm1130
echo "// JOB    1234 3333" > assemble.deck
echo "// ASM" >> assemble.deck
echo "*LIST ALL" >> assemble.deck
echo "*PRINT SYMBOL TABLE" >> assemble.deck
cat FORTH.asm >> assemble.deck
echo "// DUP" >> assemble.deck
echo "*DELETE             FORTX" >> assemble.deck
echo "*STORECI1   WS  UA  FORTX" >> assemble.deck
rm FORTH.lst
$IBM1130 <<END
deposit dsk stime 200
deposit dsk rtime 200
att dsk dms.dsk
att dsk1 forth.dsk.initialized
att prt -N FORTH.lst
att cr assemble.deck
reset
boot dsk
det prt
quit
END

