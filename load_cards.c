#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>

#define SECSZ_WDS (321)
#define SECSZ ((SECSZ_WDS) * 2)
#define DATASZ_WDS (320)
#define DATASZ  ((DATASZ_WDS) * 2)
#define CRDSZ (80)
#define CRDS_SECT (8)

#define START 238

static int fd;

struct sect_s
  {
    uint16_t sect_num;
    uint8_t data[DATASZ];
  };

// missing cents (74)
// not (95) using caret
static uint8_t table[256] =
  {
      0,   0,   0,   0,   0,   0,   0,   0, //  00
      0,   0,   0,   0,   0,   0,   0,   0, //  10
      0,   0,   0,   0,   0,   0,   0,   0, //  20
      0,   0,   0,   0,   0,   0,   0,   0, //  30
     64,  90, 127, 123,  91, 108,  80, 125, //  40 space ! " # $ % & '
     77,  93,  92,  78, 107,  96,  75,  97, //  50  ( ) * + , - . /
    240, 241, 242, 243, 244, 245, 246, 247, //  60  0 1 2 3 4 5 6 7
    248, 249, 122,  94,  76, 126, 110, 111, //  70  8 9 : ; < = > ?
    124, 193, 194, 195, 196, 197, 198, 199, // 100  @ A B C D E F G
    200, 201, 209, 210, 211, 212, 213, 214, // 110  H I J K L M N O
    215, 216, 217, 226, 227, 228, 229, 230, // 120  P Q R S T U V W
    231, 232, 233,   0,   0,   0,  95, 109, // 130  X Y Z [ \ ] ^ _
      0, 193, 194, 195, 196, 197, 198, 199, // 140  ` a b c d e f g
    200, 201, 209, 210, 211, 212, 213, 214, // 150  h i j k l m n o
    215, 216, 217, 226, 227, 228, 229, 230, // 160  p q r s t u v w
    231, 232, 233,   0,  79,   0,   0,   0, // 170  x y z { | } ~ DEL
      0,   0,   0,   0,   0,   0,   0,   0, // 200
      0,   0,   0,   0,   0,   0,   0,   0, // 210
      0,   0,   0,   0,   0,   0,   0,   0, // 220
      0,   0,   0,   0,   0,   0,   0,   0, // 230
      0,   0,   0,   0,   0,   0,   0,   0, // 240
      0,   0,   0,   0,   0,   0,   0,   0, // 250
      0,   0,   0,   0,   0,   0,   0,   0, // 260
      0,   0,   0,   0,   0,   0,   0,   0, // 270
      0,   0,   0,   0,   0,   0,   0,   0, // 300
      0,   0,   0,   0,   0,   0,   0,   0, // 310
      0,   0,   0,   0,   0,   0,   0,   0, // 320
      0,   0,   0,   0,   0,   0,   0,   0, // 330
      0,   0,   0,   0,   0,   0,   0,   0, // 340
      0,   0,   0,   0,   0,   0,   0,   0, // 350
      0,   0,   0,   0,   0,   0,   0,   0, // 360
      0,   0,   0,   0,   0,   0,   0,   0, // 370
  };

static void xlate (struct sect_s * p)
  {
    for (int i = 0; i < DATASZ; i ++)
      {
        uint8_t ch = table [p->data[i]];
        if (! ch)
          {
            printf ("bad ch %o\n", p->data[i]);
            ch = 64;
          }
        p->data[i] = ch;
      }
  }

static void unfix (struct sect_s * p)
  {
#if 0
// leave alone
return;
#endif
#if 0
// reverse card images
    for (int card = 0; card < 8; card ++)
      {
        int cp = card * 80;
        for (int col = 0; col < 40; col += 2)
          {
            uint16_t t0, t1;
            int loc = 78 - col;
            t0 = p->data[cp+col+0];
            t1 = p->data[cp+col+1];
            p->data[cp+col+0] = p->data[cp+loc+0];
            p->data[cp+col+1] = p->data[cp+loc+1];
            p->data[cp+loc+0] = t0;
            p->data[cp+loc+1] = t1;
          }
      }
#endif
#if 0
// reverse words
    for (int i = 0; i < DATASZ / 2; i += 2)
      {
        uint16_t t0, t1;
        int j = DATASZ - 2 - i;
        t0 = p->data[i+0];
        t1 = p->data[i+1];
        p->data[i+0] = p->data[j+0];
        p->data[i+1] = p->data[j+1];
        p->data[j+0] = t0;
        p->data[j+1] = t1;
      }
#endif
#if 1
// reverse bytes
    for (int i = 0; i < DATASZ / 2; i ++)
      {
        uint16_t t0;
        int j = DATASZ - 1 - i;
        t0 = p->data[i];
        p->data[i] = p->data[j];
        p->data[j] = t0;
      }
#endif
  }

static void seek (int sector)
  {
    lseek (fd, sector * SECSZ, SEEK_SET);
  }

int main (int argc, char * argv [])
  {
    struct sect_s sect;
    fd = open ("forth.dsk.initialized", O_WRONLY, 0);
    if (fd < 0)
      {
        perror ("open");
        exit (1);
      }

    int sect_num = START;
    int nbytes = 0;
    int nwritten = 0;

    memset (& sect, 0x40, sizeof (sect));
    sect.sect_num = sect_num;
    int ch;
    while ((ch = getchar ()) != EOF)
      {
        if (ch == '\n')
          {
#if 0
            while (nbytes % 80 < 79)
              {
                sect.data[nbytes ++] = ' ';
              }
#endif
            int col = nbytes % 80;
            while (col < 80)
              {
                sect.data[nbytes ++] = ' ';
                col ++;
              }
          }
        else
          {
            if (ch == ';')
              {
                while ((ch = getchar ()) != EOF)
                  if (ch == '\n')
                    break;
              }
            else
              {
                sect.data[nbytes ++] = ch;
              }
          }

        if (nbytes >= DATASZ)
          {
            xlate (& sect);
            unfix (& sect);
            seek (sect_num);
            write (fd, & sect, sizeof (sect));
//printf ("%d: ", sect_num); for (int i = 0; i < 80; i ++) printf (" %02x", sect.data[i]); printf ("\n");
            nwritten ++;
            sect_num ++;
            nbytes = 0;
            memset (& sect, 0x40, sizeof (sect));
            sect.sect_num = sect_num;
          }
      }
    printf ("residual %d\n", nbytes);
    if (nbytes)
      {
        xlate (& sect);
        unfix (& sect);
        seek (sect_num);
        write (fd, & sect, sizeof (sect));
        nwritten ++;
        sect_num ++;
      }
    printf ("written %d (%d)\n", nwritten, nwritten * CRDS_SECT);
    return 0;
  }

